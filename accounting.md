# Accounting for MiniDebConf Belo Horizonte 2024.

## Budget approved by Jonathan and Andreas

52.856,56 - 13.200,00 = 39.656,56 (received from SPI via invoice)

bursaries:travel is under 13.200,00

```
$ ./wrapper -X BRL bal -f budget.ledger
      BRL -52.856,56  assets:SPI
       BRL 58.578,96  expenses
       BRL 16.642,50    accommodation
       BRL 16.012,50      bedrooms
          BRL 630,00      room
       BRL 13.200,00    bursaries:travel
        BRL 3.347,20    daytrip
        BRL 1.947,20      food
        BRL 1.400,00      transport
       BRL 12.090,04    food
        BRL 4.386,17      coffee break
        BRL 5.226,37      dinner
        BRL 2.477,50      lunch
        BRL 1.196,36    graphic materials:general
          BRL 306,00    infra:rent
          BRL 389,18    local team:reimbursements
        BRL 9.007,68    swag
          BRL 960,00      drink cup
          BRL 640,00      lanyard
        BRL 7.407,68      t-shirt
        BRL 2.400,00    video:camcorder rental
       BRL -5.722,40  incomes
       BRL -5.000,00    sponsors:national
       BRL -3.000,00      donations
       BRL -2.000,00      silver
         BRL -722,40    supplier change
--------------------
                   0

```

## SPI accounting

Without bursaries:travel

45.378,96 - 5.722,40 (sponsorship) = 39.656,56


```
$ ./wrapper -X BRL bal

                   0  assets
       BRL 45.378,96    ICTL
      BRL -45.378,96    SPI
       BRL 45.378,96  expenses
       BRL 16.642,50    accommodation:bedrooms
        BRL 3.347,20    daytrip
        BRL 1.947,20      food
        BRL 1.400,00      transport
       BRL 12.090,04    food
        BRL 4.386,17      coffee break
        BRL 5.226,37      dinner
        BRL 2.477,50      lunch
        BRL 1.108,06    graphic materials
          BRL 153,60      badge
          BRL 791,40      banner
          BRL 163,06      sticker
          BRL 306,00    infra:rent
          BRL 389,18    local team
           BRL 18,04      food
          BRL 259,19      fuel
           BRL 10,50      general
          BRL 101,45      transport
        BRL 9.095,98    swag
           BRL 88,30      badge-bag
          BRL 960,00      drink cup
          BRL 640,00      lanyard
        BRL 7.407,68      t-shirt
        BRL 2.400,00    video:camcorder rental
      BRL -45.378,96  incomes
      BRL -39.656,56    SPI reimbursement received
       BRL -5.000,00    sponsors:national
       BRL -3.000,00      donations
       BRL -2.000,00      silver
         BRL -722,40    supplier change
--------------------
                   0

```

## Full accounting


```
$ ./wrapper -X BRL bal

        BRL 3.795,81  assets
       BRL 49.174,77    ICTL
      BRL -45.378,96    SPI
       BRL 56.822,80  expenses
        BRL 5.989,61    ICTL fees
       BRL 16.642,50    accommodation:bedrooms
        BRL 3.347,20    daytrip
        BRL 1.947,20      food
        BRL 1.400,00      transport
       BRL 12.090,04    food
        BRL 4.386,17      coffee break
        BRL 5.226,37      dinner
        BRL 2.477,50      lunch
        BRL 1.108,06    graphic materials
          BRL 153,60      badge
          BRL 791,40      banner
          BRL 163,06      sticker
          BRL 306,00    infra:rent
          BRL 858,42    local team
           BRL 19,40      food
          BRL 531,13      fuel
          BRL 161,19      general
          BRL 146,70      transport
        BRL 9.095,98    swag
           BRL 88,30      badge-bag
          BRL 960,00      drink cup
          BRL 640,00      lanyard
        BRL 7.407,68      t-shirt
        BRL 7.384,99    video
        BRL 2.400,00      camcorder rental
        BRL 4.984,99      computer
      BRL -60.618,61  incomes
      BRL -39.656,56    SPI reimbursement received
      BRL -20.239,65    sponsors:national
       BRL -2.000,00      bronze
       BRL -7.239,65      donations
       BRL -3.000,00      gold
       BRL -8.000,00      silver
         BRL -722,40    supplier change
--------------------
                   0
```
