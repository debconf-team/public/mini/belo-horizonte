# Arquivos das apresentações

Arquivos, ou links para os arquivos, usados nas apresentações da MiniDebConf
Belo Horizonte 2024.


* [Allythy Rennan - Desenvolvimento Front-End: Uma jornada com o Debian como aliado](https://slides.com/allythy/desenvolvimento-front-end-uma-jornada-com-debian-como-aliado)
* [Cláudio Henrique Pessoa Brandão & Aluizio Neto - Performance Live Eletronics com Software Livre dos repositórios Debian](https://nuvem.vilarejo.pro.br/index.php/s/5A5YiRga8NEgzw5)
