---
title: Desmistificando transi��es
author: Athos Ribeiro - CC0 1.0
...

# TOC

1. **Introdu��o**
2. Terminologia
3. Quebrando a retrocompatibilidade
4. Debian
5. Ferramentas
6. Refer�ncias


<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## Sobre mim

- Engenheiro de Software
- Canonical @ Ubuntu Server Team
- Ubuntu Core Developer
- DM

---

## Motiva��o

- Algu�m n�o veio
- Transi��es podem ser complexas; Manter bibliotecas requer maior cuidado
- Falta documenta��o sobre o assunto (kick-off?)
- Oportunidades de discutir e aprender
    - Processos da distribui��o
    - Shared objects
    - Linker/Loader

---

# TOC

1. Introdu��o
2. **Terminologia**
3. Quebrando a retrocompatibilidade
4. Debian
5. Ferramentas
6. Refer�ncias

<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## Shared libraries

Wikipedia:
> A shared library or shared object is a computer file that contains executable code designed to be used by multiple computer programs or other libraries at runtime. 

tldp.org:
> Shared libraries are libraries that are loaded by programs when they start.

- Shared objects
- Arquivos `*.so`

Carregamento din�mico!

---

### Shared libraries - Por que?

**Carregamento din�mico** => F�cil substitui��o => Seguran�a

https://lore.kernel.org/lkml/CAHk-=whs8QZf3YnifdLv57+FhBi5_WeNTG1B-suOES=RcUSmQg@mail.gmail.com/

> I really wish Fedora stopped doing that. [...]
> Shared libraries are not a good thing in general. [...]
> I think people have this incorrect picture that "shared libraries are inherently good". They really really aren't.
> [...] the advantage really should always be weighed against those (big) disadvantages.

- Linus Torvalds

> The biggest advantage of shared libraries is that they enable distributions
to provide security fixes.
> Distributions try hard to have only one place to patch and one package to
rebuild when a CVE has to be fixed.
> It is not feasible to rebuild all users of a library in a distribution every
time a CVE gets published for a library.

> Some of the new language ecosystems like Go or Rust do not offer shared
libraries.

> What happens if you use a program provided by your distribution that is
written in Rust and handles untrusted input in a way that it might be
vulnerable to exploits based on one of these CVEs?

> The program has a known vulnerability that will likely stay unfixed.

- Adrian Bunk

---

## API/ABI

### Aplication Programming Interface

Define, em n�vel de c�digo fonte, as interfaces de comunica��o entre programas
(e.g., headers `*.h`).

**Diz respeito ao c�digo fonte**

### Aplication Binary Interface

Define as interfaces de comunica��o entre bin�rios em uma dada plataforma
(e.g., como par�metros s�o passados para fun��es nos registradores, conven��es
de chamada e retorno de fun��es, etc).

**Diz respeito aos bin�rios**

--- 

## SONAME

- Shared objects (`*.so`) cont�m um campo chamado `soname`
- String: "lib" + NOME + ".so." + VERS�O
- VERS�O � incrementada quando a interface muda
- Utilizado para carregar a biblioteca (carregamento din�mico)
- soname (libicu74): `libicui18n.so.74`
- Nome real (libicu74): `libicui18n.so.74.2`
- Nome para o linker (libicu-dev): `libicui18n.so`

---

## Depend�ncia reversa

- Software **A** depende da Biblioteca **B**
- **B** � Uma depend�ncia de **A**
- **A** � Uma depend�ncia reversa de **B**

Quando falamos em depend�ncias reversas de um pacote **B**, estamos
interessados no conjunto de todos os pacotes que tem uma rela��o de depend�ncia
reversa com **B**.

---

# TOC

1. Introdu��o
2. Terminologia
3. **Quebrando a retrocompatibilidade**
4. Debian
5. Ferramentas
6. Refer�ncias


<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## Quebra de API/ABI e atualiza��o de SONAME

Quando uma mudan�a no c�digo de uma biblioteca altera um componente **j�
existente** em sua ABI, o SONAME da biblioteca deve ser alterado.

Ou seja, o SONAME deve mudar quando uma interface � removida ou quando a
assinatura de uma interface � alterada (e.g., o n�mero de par�metros ou o tipo
de par�metros recebidos foi alterado).

## Rebuilds de depend�ncias reversas

O soname � utilizado para carregar a biblioteca dinamicamente.

Se ele � alterado, mesmo sem quebra de ABI, as depend�ncias reversas precisam
ser reconstru�das para que possam utilizar a nova vers�o da biblioteca corretamente.

---

# TOC

1. Introdu��o
2. Terminologia
3. Quebrando a retrocompatibilidade
4. **Debian**
5. Ferramentas
6. Refer�ncias


<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## Transi��o

Enfim, o que � uma transi��o?

> The Release Team considers everything a transition where the upload of a package requires changes (rebuilds or actual patches) to reverse dependencies.

Fonte: https://wiki.debian.org/Teams/ReleaseTeam/Transitions

## Ferramentas importantes para a infraestrutura de transi��es

### Transition tracker (ben)

Mede o progresso de uma transi��o baseados em filtros que verificam o estado das depend�ncias reversas do pacote em transi��o

### Britney (software de migra��es [unstable -> testing])

- instalabilidade (depend�ncias satisfeitas)
- autopkgtests

---

## Processo

https://wiki.debian.org/Teams/ReleaseTeam/Transitions

- Upload para `experimental`
- Aprovado pelos FTP masters
- ben file => entrada no transition tracker
- Rebuilds de teste das depend�ncias reversas contra o novo pacote em experimental
- Abrir um bug contra release.debian.org e contra as depend�ncias reversas, mostrando os resultados das rebuilds de teste
- Release team aprova a transi��o => Upload para `unstable`. **In�cio da transi��o**
- Todos os bugs abertos promovidos para Release Critical
- binNMUs, updates, patches, remo��es
- Remo��o do pacote antigo (ABI anterior � migra��o) da `unstable`
- Britney (software de migra��es):  Nova biblioteca migra; depend�ncias reversas migram; 
- Remo��o do pacote antigo (ABI anterior � migra��o) de `testing`
- Release team fecha o bug contra release.debian.org. **Fim da transi��o**

---

# TOC

1. Introdu��o
2. Terminologia
3. Quebrando a retrocompatibilidade
4. Debian
5. **Ferramentas**
6. Refer�ncias


<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## Encontrando depend�ncias reversas

- N�o sei; como voc�s fazem?
- Ubuntu Archive Admin tools: checkreversedepends (patched)
    - https://code.launchpad.net/~athos-ribeiro/ubuntu-archive-tools/+git/ubuntu-archive-tools/+merge/462728

## Se preparando para a transi��o

https://terceiro.xyz/2021/10/12/triaging-debian-build-failure-logs-with-collab-qa-tools/

- Rebuilds de ensaio em um ambiente controlado: mass-rebuild
    - https://salsa.debian.org/terceiro/mass-rebuild
- Abrindo bugs contra depend�ncias reversas problem�ticas: collab-qa-tools
    - https://salsa.debian.org/lucas/collab-qa-tools

---

# TOC

1. Introdu��o
2. Terminologia
3. Quebrando a retrocompatibilidade
4. Debian
5. Ferramentas
6. **Refer�ncias**


<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## Refer�ncias

- https://github.com/canonical/ubuntu-maintainers-handbook/blob/main/Transitions.md
- https://wiki.debian.org/Teams/ReleaseTeam/Transitions
- https://en.wikipedia.org/wiki/Shared_library
- https://tldp.org/HOWTO/Program-Library-HOWTO/shared-libraries.html
- https://lore.kernel.org/lkml/CAHk-=whs8QZf3YnifdLv57+FhBi5_WeNTG1B-suOES=RcUSmQg@mail.gmail.com/
- https://www.debian.org/doc/debian-policy/ch-sharedlibs.html
- https://code.launchpad.net/~athos-ribeiro/ubuntu-archive-tools/+git/ubuntu-archive-tools/+merge/462728
- https://salsa.debian.org/terceiro/mass-rebuild
- https://salsa.debian.org/lucas/collab-qa-tools
- https://terceiro.xyz/2021/10/12/triaging-debian-build-failure-logs-with-collab-qa-tools/

---

# TOC

1. Introdu��o
2. Terminologia
3. Quebrando a retrocompatibilidade
4. Debian
5. Ferramentas
6. Refer�ncias
7. **B�nus: Os sistemas symbols e shlibs**


<!--config:
margins:
  left: auto
  right: auto
  top: auto
-->

---

## symbols

Mapa de todos os s�mbolos exportados pelo pacote.

FTBFS quando h� altera��es inesperadas nos s�mbolos exportados.

---

## shlibs

Rastreia a �ltima vers�o onde houve altera��o de soname.
